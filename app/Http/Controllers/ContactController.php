<?php

namespace App\Http\Controllers;

use App\About;
use App\ContactUS;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index()
    {
        $contacts = ContactUS::latest()->paginate(10);
        return view('admin.contacts.index', compact('contacts'));
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function contactUS()
    {
        // $about = $about->load(['about']);
        // return view('contact', compact('post'));
        $about = ContactUS::getAboutData();

        // Pass to view
        return view('contact')->with("about", $about);
        // return view('contact');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function contactSaveData(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'subject' => 'required',
            'message' => 'required'
        ]);

        ContactUS::create($request->all());

        return back()->with('success', 'Thanks for contacting us!');
        // return redirect()->route('contact')->with('success', 'Post deleted successfully!');
    }
}
