<?php

namespace App\Http\Controllers;

use App\Post;
use App\Comment;
use App\Category;
use App\Tag;
use Illuminate\Http\Request;

class BlogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $posts = Post::when($request->search, function ($query) use ($request) {
            $search = $request->search;
            return $query->where('title', 'like', "%$search%")->orWhere('content', 'like', "%$search%");
        })->with('user', 'category', 'tags', 'comments')->withCount('comments')->published()
            ->orderBy('created_at', 'desc')->paginate(3);

        $categories = Category::withCount('posts')->limit(10)->get();
        $tags = Tag::withCount('posts')->limit(15)->get();
        return view('post.index', compact('posts', 'categories', 'tags'));
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show(Post $post)
    {
        // $this->authorize('view', $post);
        $post = $post->load(['user', 'category', 'tags', 'comments']);
        $categories = Category::withCount('posts')->limit(5)->get();
        $tags = Tag::withCount('posts')->limit(10)->get();
        return view('post.show', compact('post', 'categories', 'tags'));
    }

    public function comment(Request $request, Post $post)
    {
        $this->validate($request, ['body' => 'required']);
        $post->comments()->create([
            'body' => $request->body
        ]);
        return redirect("/blog/post/{$post->id}")->with('success', 'Comment successfully created!');
    }

    public function post(Post $post)
    {
        $post = $post->load(['comments.user', 'tags', 'user', 'category']);
        return view('post.show', compact('post'));
    }
}
