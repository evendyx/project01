<?php

namespace App\Http\Controllers\Api;

use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\PostRequest;
use App\Http\Resources\PostResource;
use App\Http\Resources\PostResourceCollection;

class PostController extends Controller
{
    /**
     * Undocumented function
     *
     * @param Post $post
     * @return PostResource
     */

    public function show(Post $post): PostResource
    {
        return new PostResource($post);
    }

    /**
     * Undocumented function
     *
     * @return PostResourceCollection
     */

    public function index(): PostResourceCollection
    {
        return new PostResourceCollection(Post::paginate());
    }
}
