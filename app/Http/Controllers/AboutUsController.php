<?php

namespace App\Http\Controllers;

use App\About;
use App\Http\Requests\AboutRequest;
use Illuminate\Http\Request;

class AboutUsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $abouts = About::latest()->paginate(10);

        return view('admin.abouts.index', compact('abouts'));
    }

    public function show($id)
    {
        $about = About::find($id);

        return view('admin.abouts.show', compact('about'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($about->user_id != auth()->user()->id && auth()->user()->is_admin == false) {
            return redirect('/admin/about/1')->with('success', 'You cant edit other peoples post.');
        }
        $about = About::find($id);

        return view('admin.abouts.edit', compact('about'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function update(AboutRequest $request, About $about)
    {
        $about->update([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'address' => $request->address,
            'city' => $request->city,
            'country' => $request->country,
            'description' => $request->description,
        ]);

        return redirect('/admin/about/1');
    }
}
