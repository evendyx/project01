<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class ContactUS extends Model
{
    protected $table = 'contactus';

    protected $fillable = ['name', 'email', 'subject', 'message'];

    public static function getAboutData()
    {
        $value = DB::table('about')->orderBy('id', 'asc')->get();

        return $value;
    }
}
