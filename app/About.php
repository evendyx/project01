<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    public $table = 'about';

    // public $fillable = ['name', 'email', 'phone', 'address', 'city', 'country', 'description'];

    // another alternative if all fields are mass assignable
    protected $guarded = [];
}
