<?php

use App\Category;
use App\Comment;
use App\Post;
use App\Tag;
use App\User;
use Illuminate\Database\Seeder;

class DummyDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 9)->create();

        factory(Tag::class, 10)->create();

        factory(Category::class, 10)->create()
            ->each(function (Category $category) {
                factory(Post::class, 5)->create([
                    'category_id' => $category->id,
                    'user_id' => User::inRandomOrder()->pluck('id')->first(),
                ])->each(function (Post $post) {
                    $post->tags()->sync(Tag::inRandomOrder()->take(3)->get());
                });
            });

        factory(Comment::class, 40)->create([
            'post_id' => Post::inRandomOrder()->pluck('id')->first(),
            'user_id' => User::inRandomOrder()->pluck('id')->first(),
        ]);
    }
}
