@extends('layouts.dashboard')

@section('content')
<div class="container mb-5 mt-5">
    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif
    <table class="table table-bordered">
        <tr>
            {{-- <th width="80px">No</th> --}}
            <th>Name</th>
            <th>Email</th>
            <th>Subject </th>
            <th>Message </th>
        </tr>
        @foreach ($contacts as $contact)
        <tr>
            {{-- <td>1</td>
            <td>{{ $about->name }}</td>
            <td>{{ $about->email }}</td>
            <td>
                <a class="btn btn-primary btn-sm" href="{{ route('about.show',$about->id) }}">Show</a>
                <a class="btn btn-info btn-sm" href="{{ route('about.edit',$about->id) }}">Edit</a>
                {!! Form::open(['method' => 'DELETE','route' => ['about.destroy', $about->id],'style'=>'display:inline']) !!}
                <button type="submit" style="display: inline;" class="btn btn-danger btn-sm">Deleted</button>
                {!! Form::close() !!}
            </td> --}}
            <td>{{ $contact->name }}</td>
            <td>{{ $contact->email }}</td>
            <td>{{ $contact->subject }}</td>
            <td>{{ $contact->message }}</td>
        </tr>
        @endforeach
    </table>
    {!! $contacts->render() !!}
</div>
@endsection
