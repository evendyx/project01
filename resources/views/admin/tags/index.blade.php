@extends('layouts.dashboard')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 mb-5 mt-5">
            <div class="panel panel-default">
                @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
                @endif
                <div class="panel-heading">
                    <h1>Tags
                        <a href="{{ url('admin/blog/tags/create') }}" class="btn btn-info pull-right">Create New</a>
                    </h1>
                </div>
                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($tags as $tag)
                            <tr>
                                <td>{{ $tag->name }}</td>
                                <td>
                                    <a href="{{ url("/admin/blog/tags/{$tag->id}/edit") }}"
                                        class="btn btn-xs btn-info">Edit</a>
                                    <form action="{{ route('tags.destroy',$tag->id) }}" method="POST"
                                        style="display: inline;">
                                        @csrf @method('DELETE')
                                        <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="2">No tag available.</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                    {!! $tags->links() !!}
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
