@extends('layouts.dashboard')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-5 mt-5">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h1>Tags
                            <a href="{{ url('admin/blog/tags/') }}" class="btn btn-danger pull-right">Cancel</a>
                        </h1>
                    </div>
                    <div class="panel-body">
                        {!! Form::open(['url' => '/admin/blog/tags', 'class' => 'form-horizontal', 'role' => 'form']) !!}
                            @include('admin.tags._form')
                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-2">
                                    <button type="submit" class="btn btn-primary">
                                        Create
                                    </button>
                                </div>
                            </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
