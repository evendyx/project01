@extends('layouts.app')
@section('content')
<!--================ Hero sm Banner start =================-->
<section class="hero-banner hero-banner--sm mb-30px">
    <div class="container">
        <div class="hero-banner--sm__content">
            <h1>About Us</h1>
            <nav aria-label="breadcrumb" class="banner-breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">About Us</li>
                </ol>
            </nav>
        </div>
    </div>
</section>
<!--================ Hero sm Banner end =================-->
<!--================ about section start =================-->
<section class="pt-3 bg-magnolia">
    <div class="container">
        @foreach ($abouts as $about)
        <div class="row no-gutters align-items-center">
            <div class="col-md-5 mb-5 mb-md-0">
                <div class="about__content">
                    <h1>{{ $about->name }}</h1>
                    <p><strong>Email :</strong> {{ $about->email }}</p>
                    <p><strong>Phone :</strong> {{ $about->phone }}</p>
                    <p><strong>Address :</strong> {{ $about->address }}</p>
                    <p><strong>City :</strong> {{ $about->city }}</p>
                    <p><strong>Country :</strong> {{ $about->country }}</p>
                </div>
            </div>
            <div class="col-md-7">
                <div class="about__img">
                    <img class="img-fluid" src="{{asset ('img/home/about.png')}}" alt="">
                </div>
            </div>
        </div>
        <div class="row no-gutters text-justify pb-5">
            <p>{{ $about->description }}</p>
        </div>
        @endforeach
    </div>
</section>
<!--================ about section end =================-->
@endsection
