@extends('layouts.dashboard')

@section('content')
<div class="container mb-5 mt-5">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1>Edit Contact
                        <a class="btn btn-info btn-sm" href="{{ route('about.edit',$about->id) }}">Edit</a>
                    </h1>
                </div>
                <div class="panel-body">
                    <h1>{{ $about->name }}</h1>
                    <p><strong>Email: </strong> {{ $about->email }} </p>
                    <p><strong>Phone: </strong> {{ $about->phone }} </p>
                    <p><strong>Address: </strong>{{ $about->address }} </p>
                    <p><strong>City: </strong>{{ $about->city }}</p>
                    <p><strong>Country: </strong>{{ $about->country}}</p>
                    <p><strong>Descriptions: </strong></p>
                    <p style="text-align: justify;">{{ $about->description }}</p>
                </div>
                <a href="{{ url('admin/about/') }}" class="btn btn-primary pull-right">Go Back</a>
            </div>
        </div>
    </div>
</div>
@endsection
