<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    {!! Form::label('name', 'Name', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
    {!! Form::text('name', null, ['class' => 'form-control', 'required', 'autofocus']) !!}
        <span class="help-block">
            <strong>{{ $errors->first('name') }}</strong>
        </span>
    </div>
</div>
<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    {!! Form::label('email', 'Email', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
    {!! Form::textarea('email', null, ['class' => 'form-control', 'required']) !!}
        <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
    </div>
</div>
<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
    {!! Form::label('phone', 'Phone', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
    {!! Form::text('phone', null, ['class' => 'form-control', 'required', 'autofocus']) !!}
        <span class="help-block">
            <strong>{{ $errors->first('phone') }}</strong>
        </span>
    </div>
</div>
<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
    {!! Form::label('address', 'Address', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
    {!! Form::textarea('address', null, ['class' => 'form-control', 'required']) !!}
        <span class="help-block">
            <strong>{{ $errors->first('address') }}</strong>
        </span>
    </div>
</div>
<div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
    {!! Form::label('city', 'City', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
    {!! Form::textarea('city', null, ['class' => 'form-control', 'required']) !!}
        <span class="help-block">
            <strong>{{ $errors->first('city') }}</strong>
        </span>
    </div>
</div>
<div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
    {!! Form::label('country', 'Country', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
    {!! Form::text('country', null, ['class' => 'form-control', 'required', 'autofocus']) !!}
        <span class="help-block">
            <strong>{{ $errors->first('country') }}</strong>
        </span>
    </div>
</div>
<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
    {!! Form::label('description', 'Description', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
    {!! Form::textarea('description', null, ['class' => 'form-control', 'required']) !!}
        <span class="help-block">
            <strong>{{ $errors->first('description') }}</strong>
        </span>
    </div>
</div>

