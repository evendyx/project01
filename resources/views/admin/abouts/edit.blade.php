@extends('layouts.dashboard')

@section('content')
<div class="container mb-5 mt-5">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1>Edit
                        <a href="{{ url('admin/about/1') }}" class="btn btn-danger pull-right">Cancel</a>
                    </h1>
                </div>
                <div class="panel-body">
                    {!! Form::model($about, ['method' => 'PUT', 'url' => "/admin/about/{$about->id}", 'class' =>
                    'form-horizontal', 'role' => 'form']) !!}
                    @include('admin.abouts._form')
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-2">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
