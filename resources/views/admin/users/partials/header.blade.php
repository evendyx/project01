<div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center" style="background-image: url(../img/banner/banner-bg.png); background-size: cover; background-position: center top;">
    <!-- Mask -->
    <span class="mask bg-gradient-default "></span>
    <!-- Header container -->
    <div class="container-fluid d-flex align-items-center">
        <div class="row">
            <div class="col-md-12 {{ $class ?? '' }}">
                <h1 class="display-2">{{ $title }}</h1>
                @if (isset($description) && $description)
                    <p class=" mt-0 mb-5">{{ $description }}</p>
                @endif
            </div>
        </div>
    </div>
</div>
