@extends('layouts.dashboard')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 mb-5 mt-5">
            <div class="panel panel-default">
                @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
                @endif
                <div class="panel-heading">
                    <h1>Comments </h1>
                </div>
                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Post</th>
                                <th>Comment</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($comments as $comment)
                            <tr>
                                <td>{{ $comment->post->title }}</td>
                                <td>{{ $comment->body }}</td>
                                <td>
                                    <form action="{{ route('comments.destroy',$comment->id) }}" method="POST" style="display: inline;">
                                        @csrf @method('DELETE')
                                        <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="2">No comment available.</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                    {!! $comments->links() !!}
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
