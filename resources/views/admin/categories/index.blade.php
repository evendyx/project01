@extends('layouts.dashboard')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 mb-5 mt-5">
            <div class="panel panel-default">
                <div class="panel-body">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                    @endif
                    <div class="panel-heading">
                        <h1>Category
                            <a href="{{ url('admin/blog/categories/create') }}" class="btn btn-info pull-right">Create
                                New</a>
                        </h1>
                    </div>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Post Count</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($categories as $category)
                            <tr>
                                <td>{{ $category->name }}</td>
                                <td>{{ $category->posts_count }}</td>
                                <td>
                                    <a href="{{ url("/admin/blog/categories/{$category->id}/edit") }}"
                                        class="btn btn-xs btn-info">Edit</a>
                                    <form action="{{ route('categories.destroy',$category->id) }}" method="POST"
                                        style="display: inline;">
                                        @csrf @method('DELETE')
                                        <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="2">No category available.</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                    {!! $categories->links() !!}
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
