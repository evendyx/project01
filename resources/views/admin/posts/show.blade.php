@extends('layouts.dashboard')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 mt-5 mb-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1>{{ $post->title }}</h1>
                </div>
                <div class="panel-body">
                    <p style="text-align: justify;">{{ $post->content }}</p>
                    <p><strong>Category: </strong>{{ $post->category->name }}</p>
                    <p><strong>Tags: </strong>{{ $post->tags->implode('name', ', ') }}</p>
                </div>
                <a href="{{ url('admin/blog/posts/') }}" class="btn btn-info pull-right">Go Back</a>
            </div>
        </div>
    </div>
</div>
@endsection
