@extends('layouts.dashboard')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 mb-5 mt-5">
            <div class="panel panel-default">
                @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
                @endif
                <div class="panel-heading">
                    <h1>Posts
                        <a href="{{ url('admin/blog/posts/create') }}" class="btn btn-info pull-right">Create New</a>
                    </h1>
                </div>
                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Content</th>
                                <th>Author</th>
                                <th>Category</th>
                                <th>Tags</th>
                                <th>Published</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($posts as $post)
                            <tr>
                                <td>{{ $post->title }}</td>
                                <td>{{ str_limit($post->content, 50) }}</td>
                                <td>{{ $post->user->name }}</td>
                                <td>{{ $post->category->name }}</td>
                                <td>{{ $post->tags->implode('name', ', ') }}</td>
                                <td>{{ $post->published }}</td>
                                <td>
                                    @if (Auth::user()->is_admin)
                                    @php
                                    if($post->published == 'Yes') {
                                    $label = 'Draft';
                                    } else {
                                    $label = 'Publish';
                                    }
                                    @endphp
                                    <form action="{{ url("/admin/blog/posts/{$post->id}/publish") }}" method="POST"
                                        style="display: inline;">
                                        @csrf @method('PUT')
                                        <button type="submit" class="btn btn-warning btn-sm">{{ $label }}</button>
                                    </form>
                                    @endif
                                    <a class="btn btn-success btn-sm"
                                        href="{{ route('posts.show',$post->id) }}">Show</a>
                                    <a class="btn btn-info btn-sm" href="{{ route('posts.edit',$post->id) }}">Edit</a>
                                    <form action="{{ route('posts.destroy',$post->id) }}" method="POST"
                                        style="display: inline;">
                                        @csrf @method('DELETE')
                                        <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="5">No post available.</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>

                    {!! $posts->links() !!}

                </div>
            </div>
        </div>

    </div>
</div>
@endsection
