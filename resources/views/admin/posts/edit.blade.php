@extends('layouts.dashboard')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 mt-5 mb-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1>Edit Post
                        <a href="{{ url('admin/blog/posts/') }}" class="btn btn-danger pull-right">Cancel</a>
                    </h1>
                </div>
                <div class="panel-body">
                    {!! Form::model($post, ['method' => 'PUT', 'url' => "/admin/blog/posts/{$post->id}", 'class' =>
                    'form-horizontal', 'role' => 'form']) !!}
                    @include('admin.posts._form')
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-2">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
