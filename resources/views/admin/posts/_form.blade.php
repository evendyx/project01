<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
    {!! Form::label('title', 'Title', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text('title', null, ['class' => 'form-control', 'required', 'autofocus']) !!}
        <span class="help-block">
            <strong>{{ $errors->first('title') }}</strong>
        </span>
    </div>
</div>
<div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
    {!! Form::label('content', 'Content', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        {!! Form::textarea('content', null, ['class' => 'form-control', 'required']) !!}
        <span class="help-block">
            <strong>{{ $errors->first('content') }}</strong>
        </span>
    </div>
</div>

<div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
    {!! Form::label('category_id', 'Category', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        {!! Form::select('category_id', $categories, null, ['class' => 'form-control', 'required']) !!}
        <span class="help-block">
            <strong>{{ $errors->first('category_id') }}</strong>
        </span>
    </div>
</div>

@php
if(isset($post)) {
$tag = $post->tags->pluck('name')->all();
} else {
$tag = null;
}
@endphp

<div class="form-group{{ $errors->has('tags') ? ' has-error' : '' }}">
    {!! Form::label('tags', 'Tag', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        {!! Form::select('tags[]', $tags, $tag, ['class' => 'form-control select2-tags', 'required', 'multiple'])
        !!}
        <span class="help-block">
            <strong>{{ $errors->first('tags') }}</strong>
        </span>
    </div>
</div>

{{-- <div class="form-group{{ $errors->has('is_published') ? ' has-error' : '' }}">
    {!! Form::label('is_published', 'Is post published?', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        {{ Form::radio('is_published', 1, null) }} Yes, publish<br>
        {{ Form::radio('is_published',0, null) }} No, just draft
        <span class="help-block">
            <strong>{{ $errors->first('is_published') }}</strong>
        </span>
    </div>
</div> --}}
