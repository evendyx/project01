<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ url('/') }}">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Homepage</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="{{ url('admin/home') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">
    <!-- Heading -->
    <div class="sidebar-heading">Interface</div>
    <!-- Nav Item - Pages Collapse Menu -->

    <li class="nav-item">
        <a class="nav-link collapsed" href="#">
            <i class="fas fa-map-marked"></i>
            <span>MAPS</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#navbar-profile" data-toggle="collapse" role="button" aria-expanded="true"
            aria-controls="navbar-profile">
            <i class="fas fa-users"></i>
            <span>PROFILE</span>
        </a>
        <div class="collapse show" id="navbar-profile">
            <ul class="nav nav-sm flex-column">
                <li class="nav-item pl-2">
                    <a class="nav-link  pt-0 pb-3" href="{{ route('profile.edit') }}">
                        User profile
                    </a>
                </li>
                <li class="nav-item pl-2">
                    <a class="nav-link pt-0 pb-3" href="{{ route('user.index') }}">
                        User Management
                    </a>
                </li>
            </ul>
        </div>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">PAGES</div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="{{ url('/') }}">
            <i class="fas fa-fw fa-folder"></i>
            <span>HOME</span>
        </a>
    </li>
    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="{{ url('admin/about/1') }}">
            <i class="fas fa-fw fa-folder"></i>
            <span>ABOUT</span>
        </a>
    </li>
    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="{{ url('admin/features') }}">
            <i class="fas fa-fw fa-folder"></i>
            <span>FEATURES</span>
        </a>
    </li>
    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="{{ url('admin/contact') }}">
            <i class="fas fa-fw fa-folder"></i>
            <span>CONTACT US</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">BLOGS</div>
    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="{{ url('admin/blog/categories') }}">
            <i class="fas fa-th"></i>
            <span>CATEGORY</span>
        </a>
    </li>
    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="{{ url('admin/blog/comments') }}">
            <i class="fas fa-comments"></i>
            <span>COMMENTS</span>
        </a>
    </li>
    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="{{ url('admin/blog/posts') }}">
            <i class="fas fa-newspaper"></i>
            <span>POST</span>
        </a>
    </li>
    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="{{ url('admin/blog/tags') }}">
            <i class="fas fa-tags"></i>
            <span>TAGS</span>
        </a>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">
</ul>
<!-- End of Sidebar -->
