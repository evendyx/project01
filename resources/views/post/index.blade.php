@extends('layouts.app')
@section('content')
<!--================ Hero sm Banner start =================-->
<section class="hero-banner hero-banner--sm mb-30px">
    <div class="container">
        <div class="hero-banner--sm__content">
            <h1>Our Blog</h1>
            <nav aria-label="breadcrumb" class="banner-breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('home') }}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Blog</li>
                </ol>
            </nav>
        </div>
    </div>
</section>
<!--================ Hero sm Banner end =================-->

<!--================Blog Area =================-->
<section class="blog_area mb-5 mt-5">
    <div class="container">
        @if(Session::has('success'))
        <div class="row">
            <div class="col-sm-6 col-md-4 col-md-offset-4 col-sm-offset-3">
                <div id="message" class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            </div>
        </div>
        @endif
        <div class="row">
            <div class="col-lg-8">
                <div class="blog_left_sidebar">
                    @forelse ($posts as $post)
                    <article class="row blog_item">
                        <div class="col-md-3">
                            <div class="blog_info text-right">
                                <div class="post_tag">
                                    @forelse ($post->tags as $tag)
                                    <a href="#"><span class="label label-default">{{ $tag->name }}</span></a>
                                    @empty
                                    <span class="label label-danger">No tag found.</span>
                                    @endforelse
                                </div>
                                <ul class="blog_meta list">
                                    <li>
                                        <a href="">{{ $post->user->name }}
                                            <i class="lnr lnr-user"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">{{ $post->created_at->format('d/m/Y H:i')}}
                                            <i class="lnr lnr-calendar-full"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">{{ $post->comments_count }}
                                            <i class="lnr lnr-bubble"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="blog_post">
                                <img src="{{$post->image}}" alt="">
                                <div class="blog_details">
                                    <a href="{{ route('post.show',$post->id) }}">
                                        <h2>{{$post->title}} <br> <small>by {{ $post->user->name }}</small></h2>
                                    </a>
                                    <p>{{ str_limit($post->content, 200) }}</p>
                                    <a class="button button-blog" href="{{ route('post.show',$post->id) }}">View
                                        More</a>
                                </div>
                            </div>
                        </div>
                    </article>
                    @empty
                    <div class="panel panel-default">
                        <div class="panel-heading">Not Found!!</div>
                        <div class="panel-body">
                            <p>Sorry! No post found.</p>
                        </div>
                    </div>
                    @endforelse
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="blog_right_sidebar">
                    <aside class="single_sidebar_widget search_widget">
                        <div class="row form-search center">
                            {!! Form::open(['method' => 'GET', 'role' => 'form']) !!}
                            <div class="col-md-12 input-group" style="width:300px;">
                                {!! Form::text('search', request()->get('search'), ['class' => 'form-control',
                                'placeholder' => 'Search Post...']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                        <div class="br"></div>
                    </aside>
                    <aside class="single_sidebar_widget post_category_widget">
                        <h4 class="widget_title">Post Catgories</h4>
                        <ul class="list cat-list">
                            @forelse ($categories as $category)
                            <li>
                                <a href="#" class="d-flex justify-content-between">
                                    <p>{{ $category->name }}</p>
                                    <p>{{ $category->posts_count }}</p>
                                </a>
                            </li>
                            @empty
                            <li>
                                <a href="#" class="d-flex justify-content-between">
                                    <p>Not Found!</p>
                                </a>
                            </li>
                            @endforelse
                        </ul>
                        <div class="br"></div>
                    </aside>
                    <aside class="single-sidebar-widget tag_cloud_widget">
                        <h4 class="widget_title">Tag Clouds</h4>
                        <ul class="list">
                            @forelse ($tags as $tag)
                            <li><a href="#">{{ $tag->name }}</a></li>
                            @empty
                            <li><a href="#">No tag found.</a></li>
                            @endforelse
                        </ul>
                        <div class="br"></div>
                    </aside>
                    <aside class="single-sidebar-widget newsletter_widget">
                        <h4 class="widget_title">Newsletter</h4>
                        <p>
                            Here, I focus on a range of items and features that we use in life without giving them a
                            second thought.
                        </p>
                        <div class="form-group d-flex flex-row">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-envelope" aria-hidden="true"></i>
                                    </div>
                                </div>
                                <input type="text" class="form-control" id="inlineFormInputGroup"
                                    placeholder="Enter email address" onfocus="this.placeholder = ''"
                                    onblur="this.placeholder = 'Enter email'">
                            </div>
                            <a href="#" class="btn">Subcribe</a>
                        </div>
                        <p class="text-bottom">You can unsubscribe at any time</p>
                    </aside>
                </div>
            </div>
        </div>
        <div class="pagination pagination-centered">
            {!! $posts->appends(['search' => request()->get('search')])->links() !!}
        </div>
    </div>
</section>
<!--================Blog Area =================-->
@endsection
