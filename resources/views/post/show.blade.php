@extends('layouts.app')
@section('content')
<!--================ Hero sm Banner start =================-->
<section class="hero-banner hero-banner--sm mb-30px">
    <div class="container">
        <div class="hero-banner--sm__content">
            <h1>{{ $post->title }}</h1>
            <nav aria-label="breadcrumb" class="banner-breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('blog/post') }}">Blog</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Blog Details</li>
                </ol>
            </nav>
        </div>
    </div>
</section>
<!--================ Hero sm Banner end =================-->

<!--================Blog Area =================-->
<section class="blog_area single-post-area mb-5 mt-5">
    <div class="container">
        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif
        <div class="row">
            <div class="col-lg-8 posts-list">
                <div class="single-post row">
                    <div class="col-lg-12">
                        <div class="feature-img">
                            <img class="img-fluid" src="{{$post->image}}" alt="">
                        </div>
                    </div>
                    <div class="col-lg-12  col-md-12 mt-3">
                        <h2>{{ $post->title }}</h2>
                        <div class="blog_info">
                            <p class="font-italic"><strong>by {{ $post->user->name }}</strong> |
                                {{ $post->category->name }} | Post at {{ $post->created_at->format('d/m/Y H:i')}}</p>
                        </div>
                        <p style="text-align: justify;">{{ $post->content }}</p>
                    </div>
                </div>
                <div class="comments-area">
                    <h2>Comments</h2>
                    @include('post._comments')
                    @includeWhen(Auth::user(), 'post._form')
                </div>
            </div>
            <div class="col-lg-4">
                <div class="blog_right_sidebar">
                    <aside class="single_sidebar_widget author_widget">
                        <img class="author_img rounded-circle" src="{{ asset('img/blog/author.png') }}" alt="">
                        <h4>{{ $post->user->name }}</h4>
                        <p>Senior blog writer</p>
                        <div class="social_icon">
                            <a href="#">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                            <a href="#">
                                <i class="fab fa-twitter"></i>
                            </a>
                            <a href="#">
                                <i class="fab fa-github"></i>
                            </a>
                            <a href="#">
                                <i class="fab fa-behance"></i>
                            </a>
                        </div>
                        <p>Boot camps have its supporters andit sdetractors. Some people do not understand why you
                            should have to spend money on boot camp when you can get. Boot camps have itssuppor ters
                            andits detractors.</p>
                        <div class="br"></div>
                    </aside>
                    <aside class="single_sidebar_widget post_category_widget">
                        <h4 class="widget_title">Post Catgories</h4>
                        <ul class="list cat-list">
                            @forelse ($categories as $category)
                            <li>
                                <a href="#" class="d-flex justify-content-between">
                                    <p>{{ $category->name }}</p>
                                    <p>{{ $category->posts_count }}</p>
                                </a>
                            </li>
                            @empty
                            <li>
                                <a href="#" class="d-flex justify-content-between">
                                    <p>Not Found!</p>
                                </a>
                            </li>
                            @endforelse
                        </ul>
                        <div class="br"></div>
                    </aside>
                    <aside class="single-sidebar-widget tag_cloud_widget">
                        <h4 class="widget_title">Tag Clouds</h4>
                        <ul class="list">
                            @forelse ($tags as $tag)
                            <li><a href="#">{{ $tag->name }}</a></li>
                            @empty
                            <li><a href="#">No tag found.</a></li>
                            @endforelse
                        </ul>
                        <div class="br"></div>
                    </aside>
                    <aside class="single-sidebar-widget newsletter_widget">
                        <h4 class="widget_title">Newsletter</h4>
                        <p>
                            Here, I focus on a range of items and features that we use in life without giving them a
                            second thought.
                        </p>
                        <div class="form-group d-flex flex-row">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-envelope" aria-hidden="true"></i>
                                    </div>
                                </div>
                                <input type="text" class="form-control" id="inlineFormInputGroup"
                                    placeholder="Enter email address" onfocus="this.placeholder = ''"
                                    onblur="this.placeholder = 'Enter email'">
                            </div>
                            <a href="#" class="btn">Subcribe</a>
                        </div>
                        <p class="text-bottom">You can unsubscribe at any time</p>
                        <div class="br"></div>
                    </aside>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================Blog Area =================-->
@endsection
