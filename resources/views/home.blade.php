@extends('layouts.app')

@section('content')
<main class="side-main">
        <!--================ Hero sm Banner start =================-->
        <section class="hero-banner mb-30px text-center">
            <div class="container">
                <div class="row mb-5">
                    <div class="col-lg-7">
                        <div class="hero-banner__img">
                            <img class="img-fluid" src="{{ asset('img/banner/hero-banner.png') }}" alt="">
                        </div>
                    </div>
                    <div class="col-lg-12 pt-5   ">
                        <div class="hero-banner__content text-center ">
                        <h1>Advanced software made simple</h1>
                        <p>Vel aliquam quis, nulla pede mi commodo tristique nam hac Luctun torquent velit felis commodo pellentesque nulla cras vel aliqua quisan nulla pede mi commoda</p>
                        <a class="button bg mr-3" href="{{ url('/table') }}">{{ __('TABELS') }}</a>
                        <a class="button bg mr-3" href="{{ url('/summary') }}">{{ __('SUMMARY') }}</a>
                        <a class="button bg mr-3" href="{{ url('/maps') }}">{{ __('MAPS') }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--================ Hero sm Banner end =================-->
@endsection
