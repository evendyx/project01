@extends('layouts.maps')
@section('content')
<div class="wrapper">
    @include('maps.partials.sidebar')
    <div id="content">
        <div class="container-fluid">
            <button type="button" id="sidebarCollapse" class="btn btn-light btn-toggle">
                <span>Menu</span>
                <i class="fas fa-align-right"></i>
            </button>
        </div>
        <div id="mapid"></div>
    </div>
    <!-- Dark Overlay element -->
    <div class="overlay"></div>
</div>
@endsection
