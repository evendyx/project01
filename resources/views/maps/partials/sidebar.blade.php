<nav id="sidebar" class="text-right">
    <div id="dismiss">
        <i class="fas fa-arrow-right"></i>
    </div>
    <ul class="list-unstyled components text-left mt-5">
        <li class="search">
            <form class="search-form" role="search">
                <div class="form-group md-form mt-0 pt-1 waves-light">
                    <input type="text" class="form-control" placeholder="Search">
                </div>
            </form>
        </li>
        <li class="active">
            <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false"><strong>Maps</strong></a>
            <ul class=" list-unstyled" id="homeSubmenu">
                <li>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" value="">Option 1
                        </label>
                    </div>
                </li>
                <li>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" value="">Option 2
                        </label>
                    </div>
                </li>
                <li>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" value="">Option 3
                        </label>
                    </div>
                </li>
            </ul>
        </li>
        <li class="layer">
            <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false"><strong>Layer 1</strong></a>
            <ul class="collapse list-unstyled" id="pageSubmenu">
                <li>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" value="">Option 1
                        </label>
                    </div>
                </li>
                <li>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" value="">Option 2
                        </label>
                    </div>
                </li>
                <li>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" value="">Option 3
                        </label>
                    </div>
                </li>
            </ul>
        </li>
        <li class="layer">
            <a href="#pageSubmenu2" data-toggle="collapse" aria-expanded="false"><strong>Layer 2</strong></a>
            <ul class="collapse list-unstyled" id="pageSubmenu2">
                <li>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" value="">Option 1
                        </label>
                    </div>
                </li>
                <li>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" value="">Option 2
                        </label>
                    </div>
                </li>
                <li>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" value="">Option 3
                        </label>
                    </div>
                </li>
            </ul>
        </li>
        <li class="layer">
            <a href="#pageSubmenu3" data-toggle="collapse" aria-expanded="false"><strong>Layer 3</strong></a>
            <ul class="collapse list-unstyled" id="pageSubmenu3">
                <li>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" value="">Option 1
                        </label>
                    </div>
                </li>
                <li>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" value="">Option 2
                        </label>
                    </div>
                </li>
                <li>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" value="">Option 3
                        </label>
                    </div>
                </li>
            </ul>
        </li>
        <li class="layer">
            <a href="{{ url('/') }}"><strong>Back to Home</strong></a>
        </li>
    </ul>
</nav>
