@extends('layouts.app')
@section('content')
<!--================ Hero sm Banner start =================-->
<section class="hero-banner hero-banner--sm mb-5">
    <div class="container">
        <div class="hero-banner--sm__content">
            <h1>Contact Us</h1>
            <nav aria-label="breadcrumb" class="banner-breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
              </ol>
            </nav>
        </div>
    </div>
</section>
<!--================ Hero sm Banner end =================-->


<!-- ================ contact section start ================= -->
<section class="section-margin">
    <div class="container">
        @if(Session::has('success'))
        <div class="row">
            <div class="col-sm-6 col-md-4 col-md-offset-4 col-sm-offset-3">
                <div id="message" class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            </div>
        </div>
        @endif
        <div class="d-none d-sm-block mb-5 pb-4 border">
            <div id="map" style="height: 420px;"></div>
            <script>
              function initMap() {
                var uluru = {lat: -25.363, lng: 131.044};
                var grayStyles = [
                  {
                    featureType: "all",
                    stylers: [
                      { saturation: -90 },
                      { lightness: 50 }
                    ]
                  },
                  {elementType: 'labels.text.fill', stylers: [{color: '#A3A3A3'}]}
                ];
                var map = new google.maps.Map(document.getElementById('map'), {
                  center: {lat: -31.197, lng: 150.744},
                  zoom: 9,
                  styles: grayStyles,
                  scrollwheel:  false
                });
              }

            </script>
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDpfS1oRGreGSBU5HHjMmQ3o5NLw7VdJ6I&callback=initMap"></script>
        </div>

        <div class="row">
            @foreach($about as $about)
            <div class="col-md-4 col-lg-3 mb-4 mb-md-0">
              <div class="media contact-info">
                <span class="contact-info__icon"><i class="ti-home"></i></span>
                <div class="media-body">
                  <h3>{{ $about->city }}, {{ $about->country }}</h3>
                  <p>{{ $about->address }}</p>
                </div>
              </div>
              <div class="media contact-info">
                <span class="contact-info__icon"><i class="ti-headphone"></i></span>
                <div class="media-body">
                  <h3><a href="tel:{{ $about->phone }}">{{ $about->phone }}</a></h3>
                  <p>Mon to Fri 9am to 6pm</p>
                </div>
              </div>
              <div class="media contact-info">
                <span class="contact-info__icon"><i class="ti-email"></i></span>
                <div class="media-body">
                  <h3><a href="mailto:{{ $about->email }}">{{ $about->email }}</a></h3>
                  <p>Send us your query anytime!</p>
                </div>
              </div>
            </div>
            <div class="col-md-8 col-lg-9">
            @endforeach
              {{-- <form action="{{ route('contactus.store') }}" class="form-contact contact_form" action="contact_process.php" method="post" id="contactForm" novalidate="novalidate">
                <div class="row">
                  <div class="col-lg-5">
                    <div class="form-group">
                      <input class="form-control" name="name" id="name" type="text" placeholder="Enter your name">
                    </div>
                    <div class="form-group">
                      <input class="form-control" name="email" id="email" type="email" placeholder="Enter email address">
                    </div>
                    <div class="form-group">
                      <input class="form-control" name="subject" id="subject" type="text" placeholder="Enter Subject">
                    </div>
                  </div>
                  <div class="col-lg-7">
                    <div class="form-group">
                        <textarea class="form-control different-control w-100" name="message" id="message" cols="30" rows="5" placeholder="Enter Message"></textarea>
                    </div>
                  </div>
                </div>
                <div class="form-group text-center text-md-right mt-3">
                  <button type="submit" class="button button-contactForm">Send Message</button>
                </div>
              </form> --}}
                <form method="post" action="{{ route('contactus.store') }}">
                    {{ csrf_field() }}
                    <h3>Contact Us</h3>
                   <div class="row">
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                <input type="text" name="name" class="form-control" placeholder="Your Name *"  required />
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                <input type="email" name="email" class="form-control" placeholder="Your Email *"  required />
                                 @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                 @endif
                            </div>
                            <div class="form-group {{ $errors->has('subject') ? ' has-error' : '' }}">
                                <input type="text" name="subject" class="form-control" placeholder="Subject *"  />
                                @if ($errors->has('subject'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('subject') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <input type="submit" name="btnSubmit" class="btn btn-primary btn-round btn-sm" value="Send Message" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('message') ? ' has-error' : '' }}">
                                <textarea name="message" class="form-control" placeholder="Your Message *" style="width: 100%; height: 150px;" required></textarea>
                                @if ($errors->has('message'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('message') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- ================ contact section end ================= -->
@endsection
