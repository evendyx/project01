@extends('layouts.app')
@section('content')
<main class="side-main">
    <!--================ Hero sm Banner start =================-->
    <section class="hero-banner hero-banner--sm mb-30px">
        <div class="container">
          <div class="hero-banner--sm__content">
            <h1>Features Us</h1>
            <nav aria-label="breadcrumb" class="banner-breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Features Us</li>
              </ol>
            </nav>
          </div>
        </div>
    </section>
    <!--================ Hero sm Banner end =================-->

    <!--================ Feature section start =================-->
    <section class="section-margin">
        <div class="container">
          <div class="section-intro pb-85px text-center">
            <h2 class="section-intro__title">Awesome Soft Feature</h2>
            <p class="section-intro__subtitle">Vel aliquam quis, nulla pede mi commodo tristique nam hac. Luctus torquent velit felis commodo pellentesque nulla cras. Tincidunt hacvel alivquam quis nulla pede mi commodo tristique nam hac  luctus torquent</p>
          </div>

          <div class="container">
            <div class="row">
              <div class="col-lg-4">
                <div class="card card-feature text-center text-lg-left mb-4 mb-lg-0">
                  <span class="card-feature__icon">
                    <i class="ti-package"></i>
                  </span>
                  <h3 class="card-feature__title">Unique Design</h3>
                  <p class="card-feature__subtitle">Molestie lorem est faucibus faucibus erat phasellus placerat proin aptent</p>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="card card-feature text-center text-lg-left mb-4 mb-lg-0">
                  <span class="card-feature__icon">
                    <i class="ti-mouse-alt"></i>
                  </span>
                  <h3 class="card-feature__title">Business Solution</h3>
                  <p class="card-feature__subtitle">Molestie lorem est faucibus faucibus erat phasellus placerat proin aptent</p>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="card card-feature text-center text-lg-left mb-4 mb-lg-0">
                  <span class="card-feature__icon">
                    <i class="ti-headphone-alt"></i>
                  </span>
                  <h3 class="card-feature__title">Customer Support</h3>
                  <p class="card-feature__subtitle">Molestie lorem est faucibus faucibus erat phasellus placerat proin aptent</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!--================ Feature section end =================-->

@endsection
