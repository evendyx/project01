<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

// Login and register
Auth::routes();

// Homepage Landing
Route::get('/', 'IndexController@index')->name('home');

//Features
Route::get('/features', 'FeaturesController@index');

// About Us
Route::get('/about', 'AboutUsController@index');
Route::group(
    ['middleware' => 'auth'],
    function () {
        Route::resource('/admin/about', 'AboutUsController');
    }
);

// Contact Us
Route::get('/contact', 'ContactController@contactUS');
Route::post('/contact', ['as' => 'contactus.store', 'uses' => 'ContactController@contactSaveData']);

Route::group(
    ['middleware' => 'auth'],
    function () {
        Route::get('/admin/contact', 'ContactController@index');
    }
);

// Blog
Route::group(['prefix' => 'blog'], function () {
    Route::resource('/post', 'BlogsController', ['only' => ['index', 'show']]);
    Route::post('/post/{title}/comment', 'BlogsController@comment')->middleware('auth');
});

// Admin Dashboard
Route::get('/admin/home', 'HomeController@index')->name('home');

// Admin Page
Route::group(
    ['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'auth'],
    function () {
        // User Management
        Route::resource('user', 'UserController', ['except' => ['show']]);
        Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfilesController@edit']);
        Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfilesController@update']);
        Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfilesController@password']);
        // Admin Blogs
        Route::resource('/blog/posts', 'PostController');
        Route::put('/blog/posts/{post}/publish', 'PostController@publish')->middleware('admin');
        Route::resource('/blog/categories', 'CategoryController', ['except' => ['show']]);
        Route::resource('/blog/tags', 'TagController', ['except' => ['show']]);
        Route::resource('/blog/comments', 'CommentController', ['only' => ['index', 'destroy']]);
    }
);

// Summary
Route::get('/summary', 'SummaryController@index');
// Table
Route::get('/table', 'TableController@index');
// Maps
Route::get('/maps', 'MapsController@index');
