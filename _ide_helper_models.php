<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */

namespace App{
    /**
     * App\About.
     *
     * @property int                             $id
     * @property string                          $name
     * @property string                          $email
     * @property string                          $phone
     * @property string                          $address
     * @property string                          $city
     * @property string                          $country
     * @property string                          $description
     * @property \Illuminate\Support\Carbon|null $created_at
     * @property \Illuminate\Support\Carbon|null $updated_at
     * @method   static                          \Illuminate\Database\Eloquent\Builder|\App\About newModelQuery()
     * @method   static                          \Illuminate\Database\Eloquent\Builder|\App\About newQuery()
     * @method   static                          \Illuminate\Database\Eloquent\Builder|\App\About query()
     * @method   static                          \Illuminate\Database\Eloquent\Builder|\App\About whereAddress($value)
     * @method   static                          \Illuminate\Database\Eloquent\Builder|\App\About whereCity($value)
     * @method   static                          \Illuminate\Database\Eloquent\Builder|\App\About whereCountry($value)
     * @method   static                          \Illuminate\Database\Eloquent\Builder|\App\About whereCreatedAt($value)
     * @method   static                          \Illuminate\Database\Eloquent\Builder|\App\About whereDescription($value)
     * @method   static                          \Illuminate\Database\Eloquent\Builder|\App\About whereEmail($value)
     * @method   static                          \Illuminate\Database\Eloquent\Builder|\App\About whereId($value)
     * @method   static                          \Illuminate\Database\Eloquent\Builder|\App\About whereName($value)
     * @method   static                          \Illuminate\Database\Eloquent\Builder|\App\About wherePhone($value)
     * @method   static                          \Illuminate\Database\Eloquent\Builder|\App\About whereUpdatedAt($value)
     */
    class About extends \Eloquent
    {
    }
}

namespace App{
    /**
     * App\Category.
     *
     * @property int                                                  $id
     * @property string                                               $name
     * @property \Illuminate\Support\Carbon|null                      $created_at
     * @property \Illuminate\Support\Carbon|null                      $updated_at
     * @property \Illuminate\Database\Eloquent\Collection|\App\Post[] $posts
     * @property int|null                                             $posts_count
     * @method   static                                               \Illuminate\Database\Eloquent\Builder|\App\Category newModelQuery()
     * @method   static                                               \Illuminate\Database\Eloquent\Builder|\App\Category newQuery()
     * @method   static                                               \Illuminate\Database\Eloquent\Builder|\App\Category query()
     * @method   static                                               \Illuminate\Database\Eloquent\Builder|\App\Category whereCreatedAt($value)
     * @method   static                                               \Illuminate\Database\Eloquent\Builder|\App\Category whereId($value)
     * @method   static                                               \Illuminate\Database\Eloquent\Builder|\App\Category whereName($value)
     * @method   static                                               \Illuminate\Database\Eloquent\Builder|\App\Category whereUpdatedAt($value)
     */
    class Category extends \Eloquent
    {
    }
}

namespace App{
    /**
     * App\Comment.
     *
     * @property int                             $id
     * @property int                             $user_id
     * @property int                             $post_id
     * @property string                          $body
     * @property \Illuminate\Support\Carbon|null $created_at
     * @property \Illuminate\Support\Carbon|null $updated_at
     * @property \App\Post                       $post
     * @property \App\User                       $user
     * @method   static                          \Illuminate\Database\Eloquent\Builder|\App\Comment newModelQuery()
     * @method   static                          \Illuminate\Database\Eloquent\Builder|\App\Comment newQuery()
     * @method   static                          \Illuminate\Database\Eloquent\Builder|\App\Comment query()
     * @method   static                          \Illuminate\Database\Eloquent\Builder|\App\Comment whereBody($value)
     * @method   static                          \Illuminate\Database\Eloquent\Builder|\App\Comment whereCreatedAt($value)
     * @method   static                          \Illuminate\Database\Eloquent\Builder|\App\Comment whereId($value)
     * @method   static                          \Illuminate\Database\Eloquent\Builder|\App\Comment wherePostId($value)
     * @method   static                          \Illuminate\Database\Eloquent\Builder|\App\Comment whereUpdatedAt($value)
     * @method   static                          \Illuminate\Database\Eloquent\Builder|\App\Comment whereUserId($value)
     */
    class Comment extends \Eloquent
    {
    }
}

namespace App{
    /**
     * App\ContactUS.
     *
     * @property int                             $id
     * @property string                          $name
     * @property string                          $email
     * @property string                          $subject
     * @property string                          $message
     * @property \Illuminate\Support\Carbon|null $created_at
     * @property \Illuminate\Support\Carbon|null $updated_at
     * @method   static                          \Illuminate\Database\Eloquent\Builder|\App\ContactUS newModelQuery()
     * @method   static                          \Illuminate\Database\Eloquent\Builder|\App\ContactUS newQuery()
     * @method   static                          \Illuminate\Database\Eloquent\Builder|\App\ContactUS query()
     * @method   static                          \Illuminate\Database\Eloquent\Builder|\App\ContactUS whereCreatedAt($value)
     * @method   static                          \Illuminate\Database\Eloquent\Builder|\App\ContactUS whereEmail($value)
     * @method   static                          \Illuminate\Database\Eloquent\Builder|\App\ContactUS whereId($value)
     * @method   static                          \Illuminate\Database\Eloquent\Builder|\App\ContactUS whereMessage($value)
     * @method   static                          \Illuminate\Database\Eloquent\Builder|\App\ContactUS whereName($value)
     * @method   static                          \Illuminate\Database\Eloquent\Builder|\App\ContactUS whereSubject($value)
     * @method   static                          \Illuminate\Database\Eloquent\Builder|\App\ContactUS whereUpdatedAt($value)
     */
    class ContactUS extends \Eloquent
    {
    }
}

namespace App{
    /**
     * App\Post.
     *
     * @property int                                                     $id
     * @property string                                                  $title
     * @property string                                                  $content
     * @property int                                                     $category_id
     * @property int                                                     $user_id
     * @property bool                                                    $is_published
     * @property \Illuminate\Support\Carbon|null                         $created_at
     * @property \Illuminate\Support\Carbon|null                         $updated_at
     * @property string                                                  $image
     * @property \App\Category                                           $category
     * @property \Illuminate\Database\Eloquent\Collection|\App\Comment[] $comments
     * @property int|null                                                $comments_count
     * @property mixed                                                   $published
     * @property \Illuminate\Database\Eloquent\Collection|\App\Tag[]     $tags
     * @property int|null                                                $tags_count
     * @property \App\User                                               $user
     * @method   static                                                  \Illuminate\Database\Eloquent\Builder|\App\Post drafted()
     * @method   static                                                  \Illuminate\Database\Eloquent\Builder|\App\Post newModelQuery()
     * @method   static                                                  \Illuminate\Database\Eloquent\Builder|\App\Post newQuery()
     * @method   static                                                  \Illuminate\Database\Eloquent\Builder|\App\Post published()
     * @method   static                                                  \Illuminate\Database\Eloquent\Builder|\App\Post query()
     * @method   static                                                  \Illuminate\Database\Eloquent\Builder|\App\Post whereCategoryId($value)
     * @method   static                                                  \Illuminate\Database\Eloquent\Builder|\App\Post whereContent($value)
     * @method   static                                                  \Illuminate\Database\Eloquent\Builder|\App\Post whereCreatedAt($value)
     * @method   static                                                  \Illuminate\Database\Eloquent\Builder|\App\Post whereId($value)
     * @method   static                                                  \Illuminate\Database\Eloquent\Builder|\App\Post whereImage($value)
     * @method   static                                                  \Illuminate\Database\Eloquent\Builder|\App\Post whereIsPublished($value)
     * @method   static                                                  \Illuminate\Database\Eloquent\Builder|\App\Post whereTitle($value)
     * @method   static                                                  \Illuminate\Database\Eloquent\Builder|\App\Post whereUpdatedAt($value)
     * @method   static                                                  \Illuminate\Database\Eloquent\Builder|\App\Post whereUserId($value)
     */
    class Post extends \Eloquent
    {
    }
}

namespace App{
    /**
     * App\Tag.
     *
     * @property int                                                  $id
     * @property string                                               $name
     * @property \Illuminate\Support\Carbon|null                      $created_at
     * @property \Illuminate\Support\Carbon|null                      $updated_at
     * @property \Illuminate\Database\Eloquent\Collection|\App\Post[] $posts
     * @property int|null                                             $posts_count
     * @method   static                                               \Illuminate\Database\Eloquent\Builder|\App\Tag newModelQuery()
     * @method   static                                               \Illuminate\Database\Eloquent\Builder|\App\Tag newQuery()
     * @method   static                                               \Illuminate\Database\Eloquent\Builder|\App\Tag query()
     * @method   static                                               \Illuminate\Database\Eloquent\Builder|\App\Tag whereCreatedAt($value)
     * @method   static                                               \Illuminate\Database\Eloquent\Builder|\App\Tag whereId($value)
     * @method   static                                               \Illuminate\Database\Eloquent\Builder|\App\Tag whereName($value)
     * @method   static                                               \Illuminate\Database\Eloquent\Builder|\App\Tag whereUpdatedAt($value)
     */
    class Tag extends \Eloquent
    {
    }
}

namespace App{
    /**
     * App\User.
     *
     * @property int                                                                                                       $id
     * @property string                                                                                                    $name
     * @property string                                                                                                    $email
     * @property \Illuminate\Support\Carbon|null                                                                           $email_verified_at
     * @property string                                                                                                    $password
     * @property string|null                                                                                               $remember_token
     * @property \Illuminate\Support\Carbon|null                                                                           $created_at
     * @property \Illuminate\Support\Carbon|null                                                                           $updated_at
     * @property bool                                                                                                      $is_admin
     * @property \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
     * @property int|null                                                                                                  $notifications_count
     * @method   static                                                                                                    \Illuminate\Database\Eloquent\Builder|\App\User admin()
     * @method   static                                                                                                    \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
     * @method   static                                                                                                    \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
     * @method   static                                                                                                    \Illuminate\Database\Eloquent\Builder|\App\User query()
     * @method   static                                                                                                    \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
     * @method   static                                                                                                    \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
     * @method   static                                                                                                    \Illuminate\Database\Eloquent\Builder|\App\User whereEmailVerifiedAt($value)
     * @method   static                                                                                                    \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
     * @method   static                                                                                                    \Illuminate\Database\Eloquent\Builder|\App\User whereIsAdmin($value)
     * @method   static                                                                                                    \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
     * @method   static                                                                                                    \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
     * @method   static                                                                                                    \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
     * @method   static                                                                                                    \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
     */
    class User extends \Eloquent
    {
    }
}
